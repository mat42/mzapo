#ifndef MZAPO_TEMPLATE_LED_H
#define MZAPO_TEMPLATE_LED_H

#include "membase.h"
#include <unistd.h>
#include <stdint.h>
#include "hsv2rgb.h"
#include <time.h>
#include <stdio.h>
#include <math.h>

#define COLORS_LEN 2

struct LEDmode {
    struct HSV hsv_colors[COLORS_LEN];
    int fade_t;
    int shine_t;
    int phase_offset;
};

extern int modes1_num, modes2_num;
extern int cur_mode1, cur_mode2;
extern int cur_led;
extern int copy;

enum modes_update {
    INDIVIDUAL, TOGETHER, PHASE_OFFSET, FREEZE
};
extern int cur_modes_update;

#define DEFAULT_MODE_1 { /*HSV*/{{235, 1, 1}, {60, 1, 1}}, 2000, 1000, 1000}
#define DEFAULT_MODE_2 { /*HSV*/{{60, 1, 1}, {235, 1, 1}}, 1500, 1000, 1000}

extern struct LEDmode led2_modes[100];
extern struct LEDmode led1_modes[100];
void updateLEDs();
void init_led();

#endif //MZAPO_TEMPLATE_LED_H
