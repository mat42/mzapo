//
// Created by Matt on 6/3/2019.
//
#include "membase.h"

unsigned char *mem_base = NULL;

void init_membase() {
    mem_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
    if (mem_base == NULL) exit(1);
}
