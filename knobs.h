//
// Created by Matt on 6/3/2019.
//

#include <stdint.h>
#include "membase.h"

#ifndef MZAPO_TEMPLATE_KNOBS_H
#define MZAPO_TEMPLATE_KNOBS_H

volatile uint32_t rgb_knobs_value;

extern int get_rk();

extern int get_gk();

extern int get_bk();

extern int get_rb();

extern int get_gb();

extern int get_bb();

extern int is_right(int knob_d);

extern int is_left(int knob_d);

extern void read_knobs();

#endif //MZAPO_TEMPLATE_KNOBS_H
