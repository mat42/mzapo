//
// Created by Matt on 6/3/2019.
//
#include "menu.h"
#include "img/img1.h"
#include "img/img2.h"
#include "img/img3.h"

int cur_option = 1, cur_menu = HOME, font_size = 2;

char *options_home[HOME_MENU_LEN] = {"Font", "LED 1", "LED 2", "", "Exit"};
char *options_led[LED_MENU_LEN] = {"Home", "Individually", "Together", "Phase shift", "Freeze", "Effect"};
char *options_font[FONT_MENU_LEN] = {"Home", "Size 1", "Size 2", "Size 3"};
char *options_colors[COLORS_MENU_LEN] = {"Back", "Color 1", "Color 2"};
char *options_effect[EFFECT_MENU_LEN] = {0, 0, 0, 0, 0, 0, 0};

char **options = options_home;

int optionslen = HOME_MENU_LEN;

#define entry_h ((FRAME_H - MENU_V_INDENT) / optionslen)

void move_selector(int last_option, int new_option) {
    for (int i = 0; i < SEL_SIZE; i++) {
        for (int j = 0; j < SEL_SIZE; j++) {
            frame[MENU_V_INDENT + last_option * entry_h + i][SEL_X + j] = 0xffff;
            frame[MENU_V_INDENT + new_option * entry_h + i][SEL_X + j] = 0;
        }
    }
}

#define set_selector(option) move_selector(option, option)

void print_option(int i);

int menu_showed = 0;

void next_option() {
    if (!menu_showed)
        return;
    int last = cur_option;
    cur_option = cur_option == (optionslen - 1) ? 0 : cur_option + 1;
    print_option(last);
    print_option(cur_option);
}

void prev_option() {
    if (!menu_showed)
        return;
    int last = cur_option;
    cur_option = cur_option == 0 ? (optionslen - 1) : cur_option - 1;
    print_option(last);
    print_option(cur_option);
}

void alloc_option(char **option) {
    if (*option == NULL)
        *option = (char *) calloc(50, 1);
}

void show_effect_menu(int is_new_effect) {

    options_effect[0] = "Back";

    char *eff_num_format = "Effect: %d/%d";
    alloc_option(&options_effect[1]);
    sprintf(options_effect[1], eff_num_format,
            (cur_led == 0 ? cur_mode1 : cur_mode2) + 1,
            cur_led == 0 ? modes1_num : modes2_num);

    char *phase_format = "Phase offset: %d";
    alloc_option(&options_effect[2]);
    sprintf(options_effect[2], phase_format, get_cur_effect()->phase_offset);

    char *fade_format = "Fade time: %d";
    alloc_option(&options_effect[3]);
    sprintf(options_effect[3], fade_format, get_cur_effect()->fade_t);

    char *shine_format = "Shine time: %d";
    alloc_option(&options_effect[4]);

    sprintf(options_effect[4], shine_format, get_cur_effect()->shine_t);

    options_effect[5] = "Colors";
    if (is_new_effect)
        options_effect[6] = "Save";
    options = options_effect;

    optionslen = EFFECT_MENU_LEN;
    if (!is_new_effect)
        optionslen--;

    show_menu(EFFECT);
}

void show_led() {
    cur_option = cur_modes_update + 1;
    options = options_led;
    optionslen = LED_MENU_LEN;
}

void set_copy_led_option(int main_led) {
    char *str = (char *) malloc(20);
    sprintf(str, "COPY %d -> %d", main_led ? 2 : 1, main_led ? 1 : 2);
    options_home[3] = str;
}

void print_option(int i) {
    str2frame(options[i],
              MENU_V_INDENT + i * entry_h,
              MENU_H_INDENT,
              font_size,
              (i != cur_option ? 0x0 : 0xffff),
              (i == cur_option ? 0x0 : 0xffff));
}

void show_menu(int menu) {
    clear();
    menu_showed = 1;
    cur_menu = menu;
    int img_y = FRAME_H - IMAGE_H, img_x = FRAME_W - IMAGE_W;
    if (menu == FONT) {
        options = options_font;
        optionslen = FONT_MENU_LEN;
        set_selector(font_size);
        print_image(img1_data, img_x, img_y);
    }
    if (menu == HOME) {
        options = options_home;
        optionslen = HOME_MENU_LEN;
        print_image(img3_data, img_x - 30, img_y - 30);
    }
    if (menu == LED) {
        show_led();
        set_selector(cur_option);
        print_image(img2_data, img_x, img_y);
    }
    if (menu == COLORS) {
        cur_option = 1;
        options = options_colors;
        optionslen = COLORS_MENU_LEN;
        print_image(img3_data, img_x - 30, img_y - 30);
    }

    if (menu != HOME) {
        char *change_mode;
        if (copy) {
            change_mode = "COPY";
        } else if (cur_led == 0)
            change_mode = "LED 1";
        else change_mode = "LED 2";
        str2frame(change_mode, 30, 370, 2, 0xff, 0xffff);
    }

    for (int i = 0; i < optionslen; i++) {
        print_option(i);
        if (menu == COLORS && i != 0) {
            for (int ii = 0; ii < SEL_SIZE; ii++) {
                for (int jj = 0; jj < SEL_SIZE; jj++) {
                    uint16_t color = to_rgb16(get_cur_effect()->hsv_colors[i - 1]);
                    if (ii < 3 || ii >= SEL_SIZE - 3 || jj < 3 || jj >= SEL_SIZE - 3)
                        color = 0x0;
                    frame[MENU_V_INDENT + i * entry_h + ii][SEL_X + jj] = color;
                }
            }
        }
    }
    frame2lcd();
}
