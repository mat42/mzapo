//
// Created by buninmat on 5/12/19.
//

#ifndef MZAPO_COLOR_SELECTOR_H
#define MZAPO_COLOR_SELECTOR_H
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

#include "lcdframe.h"
#include "hsv2rgb.h"
#include "knobs.h"
#include "led.h"

void set_color_selector(uint16_t frame[FRAME_H][FRAME_W], struct HSV color);
void select_color(struct HSV color, struct HSV *color1, struct HSV *color2);

#endif //MZAPO_COLOR_SELECTOR_H
