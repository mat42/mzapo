/*******************************************************************
  Simple program to check LCD functionality on MicroZed
  based MZ_APO board designed by Petr Porazil at PiKRON

  mzapo_lcdtest.c       - main and only file

  (C) Copyright 2004 - 2017 by Pavel Pisa
      e-mail:   pisa@cmp.felk.cvut.cz
      homepage: http://cmp.felk.cvut.cz/~pisa
      work:     http://www.pikron.com/
      license:  any combination of GPL, LGPL, MPL or BSD licenses

 *******************************************************************/

#include <stdlib.h>
#include <stdint.h>
#include "knobs.h"
#include "lcdframe.h"
#include "color_selector.h"
#include "menu.h"

int main(int argc, char *argv[]) {
    initLED();
    init_membase();
    /*-----------LCD----------------*/
    show_logo();
    init_led();
    int rk = get_rk(), gk = get_gk(), rb = 0;
    int main_led = 0;
    set_copy_led_option(main_led);
    while (1) {
        read_knobs();
        updateLEDs();

        frame2lcd();

        int dr = rk - get_rk();
        if (is_right(dr)) {
            prev_option();
            rk = get_rk();
        }else if (is_left(dr)) {
            next_option();
            rk = get_rk();
        }

        if(cur_option == 3 && cur_menu == HOME) {
            int dg = gk - get_gk();
            if (is_right(dg)) {
                main_led = 0;
                set_copy_led_option(main_led);
                gk = get_gk();
                show_menu(HOME);
            }else if (is_left(dg)) {
                main_led = 1;
                set_copy_led_option(main_led);
                gk = get_gk();
                show_menu(HOME);
            }
        }

        int lrb = rb;
        rb = get_rb();
        if (rb || !lrb)
            continue;

        rb = 0;

        if(!menu_showed) {
            show_menu(HOME);
            continue;
        }
        switch (cur_menu) {
            case HOME:
                if (cur_option == 0) {
                    cur_option = font_size;
                    show_menu(FONT);
                } else if(cur_option == 4){
                    show_logo();
                    return 0;
                } else if(cur_option == 1 || cur_option == 2) {
                    cur_led = cur_option - 1;
                    copy = 0;
                    show_menu(LED);
                }
                else if(cur_option == 3){
                    copy = 1;
                    if(main_led == 0){
                        led1_modes[cur_mode1] = led2_modes[cur_mode2];
                    }
                    else{
                        led2_modes[cur_mode2] = led1_modes[cur_mode1];
                    }
                    show_menu(LED);
                }
                break;

            case FONT:
                if (cur_option != 0) {
                    move_selector(font_size, cur_option);
                    font_size = cur_option;
                    show_menu(FONT);
                } else
                    show_menu(HOME);
                break;

            case LED:
                if (cur_option == 0) {
                    show_menu(HOME);
                } else if (cur_option == 5) {
                    update_effect();
                } else {
                    move_selector(cur_modes_update, cur_option);
                    cur_modes_update = cur_option - 1;
                    cur_option = 1;
                    show_menu(LED);
                }
                break;

            default:
                break;
        }
    }
}
