//
// Created by Matt on 6/3/2019.
//

#ifndef MZAPO_TEMPLATE_MENU_H
#define MZAPO_TEMPLATE_MENU_H

#define HOME_MENU_LEN 5
#define LED_MENU_LEN 6
#define FONT_MENU_LEN 4
#define COLORS_MENU_LEN 3
#define EFFECT_MENU_LEN 7

#define MENU_V_INDENT 10
#define MENU_H_INDENT 50
#define SEL_SIZE 20
#define SEL_X (MENU_H_INDENT - SEL_SIZE - 10)

#include <stdint.h>
#include "lcdframe.h"
#include "hsv2rgb.h"
#include "effect.h"

extern int menu_showed;
enum menu_navigation {
    HOME, LED, FONT, COLORS, EFFECT
};
extern int cur_option, cur_menu, font_size;
void next_option();
void prev_option();

void show_menu(int menu);
void show_effect_menu(int is_new_effect);

void move_selector(int last_option, int new_option);
void set_copy_led_option(int main_led);

#endif //MZAPO_TEMPLATE_MENU_H
