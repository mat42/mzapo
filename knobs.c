//
// Created by buninmat on 6/3/19.
//
#include "knobs.h"

extern inline int get_rk() {
    return (rgb_knobs_value >> 16) & 0xFF;
}

extern inline int get_gk() {
    return (rgb_knobs_value >> 8) & 0xFF;
}

extern inline int get_bk() {
    return rgb_knobs_value & 0xFF;
}

extern inline int get_rb() {
    return (rgb_knobs_value >> 26) & 1;
}

extern inline int get_gb() {
    return (rgb_knobs_value >> 25) & 1;
}

extern inline int get_bb() {
    return (rgb_knobs_value >> 24) & 1;
}

extern inline int is_right(int knob_d) {
    return knob_d < -2 || (knob_d > 250 && knob_d < 255);
}

extern inline int is_left(int knob_d) {
    return knob_d > 2 || (knob_d < -250 && knob_d > -255);
}

extern inline void read_knobs() {
    rgb_knobs_value = *(volatile uint32_t *) (mem_base + SPILED_REG_KNOBS_8BIT_o);
}
