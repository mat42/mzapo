//
// Created by Matt on 6/3/2019.
//

#ifndef MZAPO_TEMPLATE_BASE_H
#define MZAPO_TEMPLATE_BASE_H

#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "stdlib.h"

extern unsigned char *mem_base;

void init_membase();

#endif //MZAPO_TEMPLATE_BASE_H
