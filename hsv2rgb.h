#ifndef HSV2RGB_H
#define HSV2RGB_H

#include <math.h>
#include <stdint.h>

struct RGB
{
	unsigned char R;
	unsigned char G;
	unsigned char B;
};

struct HSV
{
	double H;
	double S;
	double V;
};

struct RGB HSVToRGB(struct HSV hsv);
uint16_t to_rgb16(struct HSV color);
uint32_t to_rgb32(struct HSV color);

#endif //HSV2RGB_H